#pragma once
#include <iostream>
#include "Figure.h"

class Rook : public Figure
{
public:
	Rook(std::string place, char type);	// constractor

	virtual bool isWayClear(std::string dst);	// check if way to destination claer
	virtual bool validMove(std::string dst);	// check figure movement validity
};