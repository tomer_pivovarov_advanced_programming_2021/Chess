#include "King.h"

//king constructor
King::King(std::string place, char type)
    : Figure(place, type)
{
}

//check if way is clear
bool King::isWayClear(std::string dst)
{
    return true;  
}


//checks move and retun right code
int King::checkMove(std::string dst)
{
    char ch = Board::getCharInDst(this->getPlace());

    if (ch != this->getType())//not same type
    {
        return 2;
    }
    if (!this->emptyDest(dst) && !this->enemyInDest(dst))//allay in dest
    {
        return 3;
    }
    if (!isValidIndex(dst))//out of board range
    {
        return 5;
    }
    if (!(this->validMove(dst)))//invalid move
    {
        return 6;
    }
    if (this->getPlace() == dst)//same dest as current place
    {
        return 7;
    }
    return 0;
}

//checks king valid move
bool King::validMove(std::string dst)
{
    std::string place = this->getPlace();

    if (place[0] == dst[0] + 1 || place[0] == dst[0] - 1)//moves ledt or right
    {
        if (place[1] == dst[1] - 1 || place[1] == dst[1] || place[1] == dst[1] + 1)//moves up or down by 1 ot stay in same height
        {
            return true;
        }
    }
    else if (place[0] == dst[0])//not moving left or right
    {
        if (place[1] == dst[1] + 1 || place[1] == dst[1] - 1)//moving up or down
        {
            return true;
        }
    }

    return false;
}
