#pragma once
#include <iostream>

class Board
{
private:
	static char _board[8][8];	// the board

public:
	static void delPlace(std::string place);			// sets place as empty
	static void addPlace(std::string place, char type);	// sets place as char

	static void printBoard();					// print the board
	static char getCharInDst(std::string dst);	// return char in destination
};