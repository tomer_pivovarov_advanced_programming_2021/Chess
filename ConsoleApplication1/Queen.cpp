#include "Queen.h"
#include "Rook.h"
#include "Bishop.h"

/// <summary>
/// Constractor
/// </summary>
/// <param name="place">- The place</param>
/// <param name="type">- The type</param>
Queen::Queen(std::string place, char type)
	: Figure(place, type)	// call figure constractor
{
}

/// <summary>
/// Checks move validity
/// </summary>
/// <param name="dst">- The destination</param>
/// <returns>The protocol number</returns>
int Queen::checkMove(std::string dst)
{
	char ch;							// the character in the source
	std::string src = this->getPlace();	// the source place
	// Queen is a combination of rook and bishop
	Rook rook(src, 'r');				// create a rook
	Bishop bishop(src, 'b');			// create a bishop

	if (!isValidIndex(dst))	// check index validity
	{
		return 5;
	}
	ch = Board::getCharInDst(this->getPlace());	// get char in destination
	if (ch != this->getType())	// if char is not the same type as the figure
	{
		return 2;
	}
	if (ch < 'a')	// if it's black's turn
	{
		rook.setType('R');		// change type to black
		bishop.setType('B');	// change type to black
	}
	if (!this->emptyDest(dst) && !this->enemyInDest(dst))	// if there is an ally in destination
	{
		return 3;
	}
	if (rook.validMove(dst))	// if the movement is a valid rook move
	{
		if (!rook.isWayClear(dst))	// if the movement is a valid rook move
		{
			return 6;
		}
	}
	else if (bishop.validMove(dst))	// if the movement is a valid bishop move
	{
		if (!bishop.isWayClear(dst))	// if the movement is a valid bishop move
		{
			return 6;
		}
	}
	else if (!rook.validMove(dst) || !bishop.validMove(dst))	// if the movement is not valid bishop or rook move
	{
		return 6;
	}
	if (this->getPlace().compare(dst) == 0)	// if figure did not move
	{
		return 7;
	}
	return 0;
}

// no need to it, bishop and rook already check it
bool Queen::isWayClear(std::string dst)
{
	return true;
}

// no need to it, bishop and rook already check it
bool Queen::validMove(std::string dst)
{
	return true;
}
