#include "Pawn.h"

//pawn constructor
Pawn::Pawn(std::string place, char type)
	: Figure(place, type)
{
}

/*
this function check if was is clear
*/
bool Pawn::isWayClear(std::string dst)
{
	std::string src = this->getPlace();//get src place
	int dis = abs(dst[1] - src[1]);//get movement distance up/down(if it's first move its may be 2)
	int side = abs(dst[0] - src[0]);//get movement to right/left

	if (dst[1] > src[1] && dis > 1)//if moving down by 2
	{
		src[1]++;
		if (this->emptyDest(src) || this->emptyDest(dst))//check if place to move is empty and the place after 1 tile too
		{
			return true;
		}
	}
	else if (dst[1] < src[1] && dis > 1)//same but with moving up
	{
		src[1]--;
		if (this->emptyDest(src) || this->emptyDest(dst))
		{
			return true;
		}
	}
	else if (side == 0)
	{
		if (this->emptyDest(dst))//if moning foraward and not eating
		{
			return true;
		}
	}
	else if (side == 1)
	{
		if (this->enemyInDest(dst))//only can move to sides if eating an enemy
		{
			return true;
		}
	}

	return false;
}

//this function checks valid move of pawn	
bool Pawn::validMove(std::string dst)
{
	bool white = false;
	bool first = false;
	std::string src = this->getPlace();//gets current position

	if (this->getType() > 'a')//check if figure is white
	{
		white = true;
	}

	if (white)
	{
		if (src[1] == '2')//check if in starting position
		{
			first = true;
		}

		if (src[1] + 1 == dst[1])//if  moving up/down by 1
		{
			return true;
		}
		else if (src[1] + 2 == dst[1] && first)//can move by 2 only if first move
		{
			return true;
		}
	}
	else
	{
		if (src[1] == '7')//check if in starting position
		{
			first = true;
		}

		if (src[1] - 1 == dst[1])//if  moving up/down by 1
		{
			return true;
		}
		else if (src[1] - 2 == dst[1] && first)//can move by 2 only if first move
		{
			return true;
		}
	}

	return false;
}
