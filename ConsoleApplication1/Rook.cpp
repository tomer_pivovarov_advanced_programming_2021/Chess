#include "Rook.h"

#define MAX_INDEX_ROW '8'
#define MIN_INDEX_ROW '0'
#define MAX_INDEX_COL 'h'
#define MIN_INDEX_COL 'a'



/// <summary>
/// Constractor
/// </summary>
/// <param name="place">- The place</param>
/// <param name="type">- The type</param>
Rook::Rook(std::string place, char type)
	: Figure(place, type)	// call figure constractor
{
}

/// <summary>
/// Checks if way to destination is clear
/// </summary>
/// <param name="dst">- The destination</param>
/// <returns>true - if the way is clear</returns>
bool Rook::isWayClear(std::string dst)
{
	int check;	// the number of places to check
	int dir = 0;	// the direction
	std::string place = this->getPlace();	// the source place

	if (place[0] == dst[0])	// if is moving vertically
	{
		if (place[1] > dst[1])	// if moving up
		{
			dir = 2; // change direction to up
			check = place[1] - dst[1];	// get number of places to check
		}
		else	// moving down
		{
			dir = 1; // change direction to down
			check = dst[1] - place[1];	// get number of places to check
		}
	}
	else	// if not moving vertically
	{
		if (place[0] > dst[0])	// if moving left
		{
			dir = 3; // change direction to left
			check = place[0] - dst[0];	// get number of places to check
		}
		else	// if moving right
		{
			dir = 4; // change direction to right
			check = dst[0] - place[0];	// get number of places to check
		}
	}

	for (int i = 0; i < check - 1; i++)	// for every place between the source and destination
	{
		switch (dir)	// switch the direction
		{
		case 1:	// down
			place[1]++;	// change the place to check
			break;

		case 2:	// up
			place[1]--;	// change the place to check
			break;

		case 3:	// left
			place[0]--;	// change the place to check
			break;

		case 4:	// right
			place[0]++;	// change the place to check
			break;

		default:	// not moving right
			std::cout << "rook check way error!" << std::endl;
		}
		if (!this->emptyDest(place))	// if the check is not empty
		{
			return false;
		}
	}
	return true;
}

/// <summary>
/// Checks figure movement validity
/// </summary>
/// <param name="dst">- The destination</param>
/// <returns>true - if the figure moved as it should</returns>
bool Rook::validMove(std::string dst)
{
	std::string place = this->getPlace();	// the source place of the figure

	if (place[0] == dst[0])	// if not moving to the sides
	{
		if (dst[1] > MIN_INDEX_ROW && dst[1] <= MAX_INDEX_ROW && dst[1] != place[1])	// check validity of index for figure movement
		{
			return true;
		}
	}
	if (place[1] == dst[1])	// if moving vertically
	{
		if (dst[0] >= MIN_INDEX_COL && dst[0] <= MAX_INDEX_COL && dst[0] != place[0])	// check validity of index for figure movement
		{
			return true;
		}
	}
	return false;

}
