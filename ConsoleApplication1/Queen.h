#pragma once
#include <iostream>
#include "Figure.h"

class Queen : public Figure
{
public:
	Queen(std::string place, char type);	// constractor

	virtual int checkMove(std::string dst);		// check move validity
	virtual bool isWayClear(std::string dst);	// check if way to destination is clear
	virtual bool validMove(std::string dst);	// check figure movement validity
};