#pragma once
#include <iostream>
#include "Figure.h"

class King : public Figure
{
public:
	King(std::string place, char type);	// constractor
	
	virtual bool isWayClear(std::string dst);	// need to declare to not cause error
	virtual int checkMove(std::string dst);		// check move validity
	virtual bool validMove(std::string dst);	// check figure movement validity
};