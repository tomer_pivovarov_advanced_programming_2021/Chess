#pragma once
#include <iostream>
#include "Figure.h"

class Bishop : public Figure
{
public:
	Bishop(std::string place, char type);	// constractor

	virtual bool validMove(std::string dst);	// check figure movement validity
	virtual bool isWayClear(std::string dst);	// check way to destination
};