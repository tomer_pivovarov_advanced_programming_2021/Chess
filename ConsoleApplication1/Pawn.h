#pragma once
#include <iostream>
#include "Figure.h"

class Pawn : public Figure
{
public:
	Pawn(std::string place, char type);	// constractor

	virtual bool isWayClear(std::string dst);	// check if way to destination is clear
	virtual bool validMove(std::string dst);	// check figure movement validity
};