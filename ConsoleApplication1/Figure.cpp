#include "Figure.h"

/// <summary>
/// Simple constractor
/// </summary>
/// <param name="place">- The place of the figure</param>
/// <param name="type">- The type of the figure</param>
Figure::Figure(std::string place, char type)
	:_place(place), _type(type)
{
}

/// <summary>
/// Check move validity and move if valid
/// </summary>
/// <param name="dst">- The destination to move to</param>
/// <returns>0 - if moved, other protocol number if error</returns>
int Figure::move(std::string dst)
{
	int ret = this->checkMove(dst);	// check validity of move
	if (ret == 0)	// if move valid
	{
		// move the figure
		Board::addPlace(dst, this->_type);
		Board::delPlace(this->_place);
	}
	else
	{
		std::cout << "Invalid move\n";
	}

	return ret;
}

/// <summary>
/// Check move validity
/// </summary>
/// <param name="dst">- The place to move to</param>
/// <returns>The protocol number</returns>
int Figure::checkMove(std::string dst)
{
	char ch;	// will be character in destination

	if (!isValidIndex(dst))	// if index out of range
	{
		return 5;
	}
	ch = Board::getCharInDst(this->getPlace());
    if (ch != this->getType())	// if not the type of the figure
    {
        return 2;
    }
    if (!this->emptyDest(dst) && !this->enemyInDest(dst))	// allay figure in the destination
    {
        return 3;
    }
    if (this->validMove(dst))	// if movment is valid for the figure
    {
		if (!this->isWayClear(dst))	// if there is a figure in the way
		{
			return 6;
		}
    }
	else if(!(this->validMove(dst)))	// if movement is not valid for the figure
	{
		return 6;
	}
    if (this->getPlace().compare(dst) == 0)	// if figure not moved
    {
        return 7;
    }
    return 0;	// if all ok
}

/// <summary>
/// Checks if index is valid
/// </summary>
/// <param name="dst">- The destination to move to</param>
/// <returns>true - if the index is valid</returns>
bool Figure::isValidIndex(std::string dst)
{
	if (dst.length() == 2)	// check index length validity
	{
		if ((dst[0] <= 'h' && dst[0] >= 'a') && (dst[1] > '0' && dst[1] <= '8'))	// check index substring validity
		{
			return true;
		}
	}
	return false;
}

/// <summary>
/// Check if the destination is empty
/// </summary>
/// <param name="dst">- The destination index</param>
/// <returns>true - if the destination is empty</returns>
bool Figure::emptyDest(std::string dst)
{
	if (Board::getCharInDst(dst) == '#')	// if destination is empty
	{
		return true;
	}
	return false;
}

/// <summary>
/// Check if there is enemy in destination
/// </summary>
/// <param name="dst">- The destination</param>
/// <returns>true - if there is enemy in the destination</returns>
bool Figure::enemyInDest(std::string dst)
{
	bool white = false;	// who's turn it is
	char ch = Board::getCharInDst(dst);	// what is the char in the destination

	if (ch == '#')	// if destination is empty
	{
		return false;
	}

	if (this->_type > 'a')	// if the player figure is white (small letter)
	{
		white = true;
	}

	if (white)	// if it's the white's turn
	{
		if (ch < 'a')	// if the destination character is black (big letter)
		{
			return true;
		}
	}
	else	// if it's the black's turn
	{
		if (ch > 'a')	// if the destination character is white (small letter)
		{
			return true;
		}
	}
	return false;
}

/// <summary>
/// Place setter
/// </summary>
/// <param name="place">- The place to set</param>
void Figure::setPlace(std::string place)
{
	this->_place = place;
}

/// <summary>
/// Type setter
/// </summary>
/// <param name="type">- The type to set</param>
void Figure::setType(char type)
{
	this->_type = type;
}

/// <summary>
/// Place getter
/// </summary>
/// <returns>The place</returns>
std::string Figure::getPlace()
{
	return this->_place;
}

/// <summary>
/// Type getter
/// </summary>
/// <returns>The type</returns>
char Figure::getType()
{
	return this->_type;
}
