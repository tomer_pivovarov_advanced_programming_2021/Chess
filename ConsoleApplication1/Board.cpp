#include "Board.h"

#define LITTLE_A 'a'
#define ASCCI_ZERO '0'
#define EMPTY '#'
#define BOARD_SIDE 8

// Initialize board
char Board::_board[8][8] = { { 'r','n','b','k','q','b','n','r' },
							 { 'p','p','p','p','p','p','p','p' },
							 { '#','#','#','#','#','#','#','#' },
							 { '#','#','#','#','#','#','#','#' },
							 { '#','#','#','#','#','#','#','#' },
							 { '#','#','#','#','#','#','#','#' },
							 { 'P','P','P','P','P','P','P','P' },
							 { 'R','N','B','K','Q','B','N','R' } };

/// <summary>
/// Set place as empty
/// </summary>
/// <param name="place">- The place to set</param>
void Board::delPlace(std::string place)
{
	_board[place[1] - ASCCI_ZERO - 1][place[0] - LITTLE_A] = EMPTY;
}

/// <summary>
/// Set place as given char
/// </summary>
/// <param name="place">- The place to set into</param>
/// <param name="type">- The char to set</param>
void Board::addPlace(std::string place, char type)
{
			_board[place[1] - ASCCI_ZERO - 1][place[0] - LITTLE_A] = type;
}

/// <summary>
/// Prints the board
/// </summary>
void Board::printBoard()
{
	for (int i = 0; i < BOARD_SIDE; i++)		// for every line in the board array
	{
		for (int j = 0; j < BOARD_SIDE; j++)	// for every char in the line
		{
			std::cout << _board[i][j] << ' ';	// print the char and space
		}
		std::cout << std::endl;					// print breakline after the line
	}
}

/// <summary>
/// Get char in destination
/// </summary>
/// <param name="dst">- The destination to take the char from</param>
/// <returns></returns>
char Board::getCharInDst(std::string dst)
{
	return Board::_board[dst[1] - ASCCI_ZERO - 1][dst[0] - LITTLE_A];
}

