#pragma once
#include <iostream>
#include "Figure.h"

class Knight : public Figure
{
public:
	Knight(std::string place, char type);	// constractor

	virtual int checkMove(std::string dst);		// check move validity
	virtual bool validMove(std::string dst);	// check figure movement validity
	virtual bool isWayClear(std::string dst);	// to not cause error
};