#pragma once
#include <iostream>
#include "Board.h"

class Figure : public Board
{
private:
	std::string _place;	// the place of the figure
	char _type;			// the type of the figure

public:
	//Constractor
	Figure(std::string place, char type);

	// Move func
	int move(std::string dst);

	// Move checkers
	virtual int checkMove(std::string dst);
	static bool isValidIndex(std::string dst);
	virtual bool isWayClear(std::string dst) = 0;
	bool emptyDest(std::string dst);
	bool enemyInDest(std::string dst);
	virtual bool validMove(std::string dst) = 0;
	
	// Setters
	void setPlace(std::string place);
	void setType(char type);
	
	// Getters
	std::string getPlace();
	char getType();

};