#include "Knight.h"
//knight constructor
Knight::Knight(std::string place, char type)
    : Figure(place, type)
{
}

//check movement of knight and return movement code
int Knight::checkMove(std::string dst)
{
    char ch;

    if (!isValidIndex(dst))//out of board range
    {
        return 5;
    }
    ch = Board::getCharInDst(this->getPlace());
    if (ch != this->getType())//not same figure
    {
        return 2;
    }
    if (!this->emptyDest(dst) && !this->enemyInDest(dst))//allay in dest
    {
        return 3;
    }
    if (!(this->validMove(dst)))//invalid move
    {
        return 6;
    }
    if (this->getPlace().compare(dst) == 0)//not the same dest as place
    {
        return 7;
    }
    return 0;
}

//checks valid move of knight
bool Knight::validMove(std::string dst)
{
    std::string src = this->getPlace();

    if (dst[0] == src[0] + 2 || dst[0] == src[0] - 2)//if moves right/left by 2 
    {
        if (dst[1] == src[1] + 1 || dst[1] == src[1] - 1)//if moves up/down by 2
        {
            return true;
        }
    }
    if (dst[1] == src[1] + 2 || dst[1] == src[1] - 2)//if move up/down by 2
    {
        if (dst[0] == src[0] + 1 || dst[0] == src[0] - 1)//if move right/left by 1
        {
            return true;
        }
    }
    return false;
}

//always way clear
bool Knight::isWayClear(std::string dst)
{
    return true;
}
