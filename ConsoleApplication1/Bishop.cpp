#include "Bishop.h"

//bishop constructor-uses figures
Bishop::Bishop(std::string place, char type)
	: Figure(place, type)
{
}
//check valid move of bishop
bool Bishop::validMove(std::string dst)
{
	std::string src = this->getPlace();

	if (abs(dst[0] - src[0]) == abs(dst[1] - src[1]))//if moving by slant only
	{
		return true;
	}

	return false;
}

//check if the way to the dst is clear  
bool Bishop::isWayClear(std::string dst)
{
	std::string src = this->getPlace();
	bool up = false;
	bool right = false;
	int i = abs(src[0] - dst[0]);//get the distance

	if (dst[1] > src[1])
	{
		up = true;//set movement up to true
	}
	if (dst[0] > src[0])
	{
		right = true;//set movement right to true
	}

	for (i = i; i > 1; i--)
	{
		if (up)
		{
			src[1]++;//move the place upward
		}
		else
		{
			src[1]--;//move place down
		}
		if (right)
		{
			src[0]++;//move the place right
		}
		else
		{
			src[0]--;//move the place left
		}

		if (!this->emptyDest(src))//check if place is not empty
		{
			return false;
		}
	}

	return true;
}
