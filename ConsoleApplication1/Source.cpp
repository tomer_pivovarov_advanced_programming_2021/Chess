/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include "Bishop.h"
#include "Board.h"
#include "Figure.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"
#include "Queen.h"
#include "Rook.h"
#include <vector>
#include <iostream>
#include <thread>

#define BOARD_SIZE 8

using std::cout;
using std::endl;
using std::string;

int typeInVector(char ch);
bool checkChess(int turn, std::vector<Figure*> f, std::string enemyKing);
bool checkSelfChess(int turn, std::vector<Figure*> f, std::string enemyKing, std::string place, char type);

void main()
{
	srand(time_t(NULL));

	
	Pipe p;
	bool isConnect = p.connect();
	char type;

	string lastPosBK = "d8"; // later d1
	string lastPosWK = "d1"; // later d8

	bool ok = false;
	bool isChess = false;
	int turnCount = 0;
	int placeInVector = 0;
	int check = 0;
	char dstType;
	string src = "";
	string dst = "";
	string ans;
	char toSend[1024];

	King king("00", 'k');
	Rook rook("00", 'r');
	Knight knight("00", 'n');
	Bishop bishop("00", 'b');
	Queen queen("00", 'b');
	Pawn pawn("00", 'p');

	//this vector uses to have the pointers to all the figures
	std::vector<Figure*> f = { &rook, &king, &knight, &bishop, &queen, &pawn };


	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(500);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1"); // just example...
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();
	Board::printBoard();
	while (msgFromGraphics != "quit")
	{
		
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4  (move e2 to e4)
		src = "";
		dst = "";
		src += msgFromGraphics[0];
		src += msgFromGraphics[1];// get the source place
		dst += msgFromGraphics[2];
		dst += msgFromGraphics[3];//get the destination place

		type = Board::getCharInDst(src);//get the type of the figure in source position

		if (turnCount % 2 == 1)//check if its white move
		{
			if (type > 'a' && type != '#')//if white figure
			{
				ok = true;//set as good choice
			}
			else
			{
				ok = false;//not a white figure or its empty place
			}
		}
		else//check fo black figure
		{
			if (type < 'a' && type != '#')
			{
				ok = true;
			}
			else
			{
				ok = false;
			}
		}
		if (ok)
		{
			placeInVector = typeInVector(type);//get the place in the vector

			f[placeInVector]->setPlace(src);//set the place of the current figure in its pointer in the vector
			f[placeInVector]->setType(type);//set it's type


			dstType = Board::getCharInDst(dst);//get the char in the destination

			check = f[placeInVector]->move(dst);// check the movment and move the figure

			if (check == 0)//if valid move
			{
				if (type == 'k')
				{
					lastPosWK = dst;//update the king's location if its white king movement
				}
				else if (type == 'K')
				{
					lastPosBK = dst;//update the king's location if its black king movement
				}

				if (turnCount % 2 == 1)//if white turn
				{
					if (!checkSelfChess(turnCount, f, lastPosWK, dst, dstType))//check that self chess is not accured
					{
						isChess = checkChess(turnCount, f, lastPosBK);//check chess on opponent
					}
					else
					{
						check = 4;//set check to chess code
						if (type == 'k')
						{
							lastPosWK = f[1]->getPlace();
						}
						turnCount++;//change the turn to black's
					}
				}
				else
				{
					if (!checkSelfChess(turnCount, f, lastPosBK, dst, dstType))
					{
						isChess = checkChess(turnCount, f, lastPosWK);
					}
					else
					{
						check = 4;
						if (type == 'K')
						{
							lastPosBK = f[1]->getPlace();
						}
						turnCount++;
					}
				}
				if (isChess && check != 4)
				{
					check = 1;
				}
				turnCount++;//change the turn
			}
		}
		else
		{
			check = 2;//invalid move,not payer's turn
		}
		toSend[0] = (char)(check + '0');//add the movment code to the string
		toSend[1] = NULL;

		strcpy_s(msgToGraphics,toSend ); // msgToGraphics should contain the result of the operation
		Board::printBoard();
		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();

		
	}
	p.close();
}


/*
	This function returns the index in the vector of figures
	of specific figure by it's char
*/
int typeInVector(char ch)
{
	switch (ch)
	{
	//if rook
	case 'r':
	case 'R':
		return 0;
		break;

	//if king
	case 'k':
	case 'K':
		return 1;
		break;
	//if knight
	case 'n':
	case 'N':
		return 2;
		break;
	//if bishop
	case 'b':
	case 'B':
		return 3;
		break;
	//if queen
	case 'q':
	case 'Q':
		return 4;
		break;
	//if pawn
	case 'p':
	case 'P':
		return 5;
		break;
	//else
	default:
		return -1;
		break;
	}
}
/*
	This function checks chess and return the result
*/
bool checkChess(int turn, std::vector<Figure*> f, std::string enemyKing)
{
	bool white = (turn % 2 == 1) ? true : false;//check if its white turn
	std::string dest;
	char ch;
	int placeInVec;
	std::string originPlace;

	for (int i = 0; i < f.size(); i++)//loop the figure's vector 
	{
		ch = f[i]->getType();//get the type of the current figure
		if (ch < 'a' && white)//check if figure in white
		{
			f[i]->setType(ch + 32);//change the color type of the figure(white to black)
		}
		else if (ch > 'a' && !white)
		{
			f[i]->setType(ch - 32);//change the color type of the figure(black to white)
		}
	}
	//loop all the board
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			//getting the current position in the board
			dest = "";
			dest += char(i + 'a');
			dest += char(j + '1');

			ch = Board::getCharInDst(dest);

			if (ch != '#')//if not empty place
			{
				placeInVec = typeInVector(ch);//get the place in the vector for the current figure
				f[placeInVec]->setPlace(dest);//set place to destination for check
				if (f[placeInVec]->checkMove(enemyKing) == 0)//if its valid move (accures a chess)
				{
					return true;
				}
			}
		}
	}
	return false;
}

/*
	this function checks self chess of a figure 
	by making her move and check if chess is accured to himself 
*/
bool checkSelfChess(int turn, std::vector<Figure*> f, std::string enemyKing, std::string place, char type)
{
	char ch = Board::getCharInDst(place);//gets the char of the figure we want to move
	std::string currPlace;
	int placeInVec = 0;
	placeInVec = typeInVector(ch);
	currPlace = f[placeInVec]->getPlace();//get it's current place

	if (checkChess(turn + 1, f, enemyKing))//check chess for the other team
	{
		f[placeInVec]->setPlace(currPlace);//return the figure to the place before moving
		std::cout << "Self chess!" << std::endl;
		std::cout << "Invalid Move!" << std::endl;
		Board::addPlace(f[placeInVec]->getPlace(), ch);//change to place on the board
		Board::addPlace(place, type);//same
		return true;//return self chess
	}
	return false;
}
